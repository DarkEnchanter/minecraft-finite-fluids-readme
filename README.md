#Welcome to Minecraft Finite Fluids!

##Introduction

This is a project to make minecraft have more realistic water. The idea originated back with [djoslin's FiniteLiquid Mod](http://http://www.djoslin.info/projects/minecraft-mods/finiteliquid/), but the mod has not been updated since minecraft 1.5.1. As a result, the Realistic Fluids Overhaul mod was created to take the torch. But alas, the developer of RFO has not had the time to update his mod very much, so finally keybounce created this project. The mod changes the nature of liquids in Minecraft. While traditionally, minecraft liquids are endless, this mod makes them distribute themselves to fill the container.

##Installation
*Note: You must have [Java 7](http://www.oracle.com/technetwork/java/javase/downloads/java-archive-downloads-javase7-521261.html) to run this. Java 8 does not work.*

####**WARNING: THIS MOD IS IN EARLY ALPHA.** There are still many bugs and this mod could destroy your worlds, so **ALWAYS BACK UP YOUR SAVES.** There is nothing we can do to save corrupted save files.


 - To install this mod *at your own risk*, go to the [downloads page](https://bitbucket.org/keybounce/minecraft-finite-fluids/downloads/) and select the most recent version (Currently Alpha 4.0). You also need [Minecraft Forge](http://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.7.10.html).
 -  Install and run Forge 1.7.10 once.
 -- To install Forge, you must run Minecraft 1.7.10 at least once.
 - Go into the minecraft folder and open the mods folder.
 - Drag the downloaded file into the mods folder and run minecraft.
 
##Screenshots


![Taken from original RFO page on minecraftforums](http://puu.sh/atmxQ/bb11b2d98b.jpg)